import gtk.gdk
from jewelenum import JewelEnum as JE
from thresholds import HSV_Jewels, RGB_Jewels

class JewelSampler(object):
    def __init__(self):
        #grid parameters
        self.x = 0
        self.y = 0
        self.xstep = 10 #how far apart (x) each point is
        self.ystep = 10 #how far apart (y) each point is
        self.rows = 8
        self.cols = 8
        self.targetwindow = gtk.gdk.get_default_root_window()
        self.current_pixel_buffer = None
        self.CS = gtk.gdk.COLORSPACE_RGB
        self.grab_width = 100
        self.grab_height = 100
        self.sample_width = 5
        self.sample_height = 5
        self.dump_images = False
        self.samplearray = []
        #initialise the array
        for col in range(self.cols):
            self.samplearray.append([])
            for row in range(self.rows):
                self.samplearray[col].append(None)
        self.jewelarray = []
        for col in range(self.cols):
            self.jewelarray.append([])
            for row in range(self.rows):
                self.jewelarray[col].append(None)


    def _get_window_contents(self):
        self.current_pixel_buffer = gtk.gdk.Pixbuf(self.CS, False, 8, self.x + self.grab_width, self.y + self.grab_height)
        self.current_pixel_buffer = self.current_pixel_buffer.get_from_drawable(self.targetwindow, self.targetwindow.get_colormap(), self.x, self.y, 0,0,self.grab_width, self.grab_height)

    def expandThreshold(self, colour):
        rgb = RGB_Jewels[colour]
        hsv = HSV_Jewels[colour]
        #expand all thresholds by 10%
        rgb[3] += 0.1*rgb[3]
        rgb[4] += 0.1*rgb[4]
        rgb[5] += 0.1*rgb[5]

        hsv[3] += 0.1*hsv[3]
        hsv[4] += 0.1*hsv[4]
        hsv[5] += 0.1*hsv[5]
        print colour, '+10% = HSV:', hsv, ', RGB:', rgb


    def avg_hsvrgb(self, data):
        hsv_list = []
        rgb_list = []
        cols = len(data)
        rows = len(data[0])
        l = float(cols * rows)
        
        #use all points
        for col in range(cols):
            hsvs = map(self.get_hsv, data[col])
            for entry in hsvs:
                hsv_list.append(entry)
            for row in range(rows):
                rgb_list.append(data[col][row])

        from numpy import *
        hsv = array(hsv_list)
        rgb = array(rgb_list)

        #hue
        h = hsv[0: , :1]
        avg_h = sum(h)/float(len(h))

        #saturation
        s = hsv[0: , 1:2]
        avg_s = sum(s)/float(len(s))

        #value
        v = hsv[0: , 2:]
        avg_v = sum(v)/float(len(v))

        #red
        r = rgb[0: , :1]
        avg_r = sum(r)/float(len(r))

        #green
        g = rgb[0: , 1:2]
        avg_g = sum(g)/float(len(g))

        #blue
        b = rgb[0: , 2:]
        avg_b = sum(b)/float(len(b))

        retval = (avg_h, avg_s, avg_v, avg_r, avg_g, avg_b)
        #print 'Averages were: ', retval
        return retval 


    def _normaliseThresholdColour(self, colour, board):
        from numpy import *
        col_list = []
        for row in range(self.rows):
            for col in range(self.cols):
                if board[col][row] == colour:
                    col_list.append(self.samplearray[col][row])
       
        stats_table = []

        #for each location containing that colour
        for data in col_list:
            #push into the stats table
            stats_table.append( self.avg_hsvrgb(data) )

        #right. now our stats table holds a bunch of data. We want to normalise it
        
        #if it has no data, return None
        if len(stats_table) == 0:
            return None
       
        #if we are a sparkly, dont normalise. We need to stay wiiiide open.
        words = colour.split('_')
        if len(words) == 2 and words[1] == 'sp':
            return None


        stats_table = array(stats_table)
        #and now I want the min, max, avg of all of these (18 values)
        h = stats_table[0: , :1]
        s = stats_table[0: , 1:2]
        v = stats_table[0: , 2:3]
        r = stats_table[0: , 3:4]
        g = stats_table[0: , 4:5]
        b = stats_table[0: , 5:]

        #avg
        avg_h = sum(h)/float(len(h))
        avg_s = sum(s)/float(len(s))
        avg_v = sum(v)/float(len(v))
        avg_r = sum(r)/float(len(r))
        avg_g = sum(g)/float(len(g))
        avg_b = sum(b)/float(len(b))

        #min
        min_h = min(h).tolist()[0]
        min_s = min(s).tolist()[0]
        min_v = min(v).tolist()[0]
        min_r = min(r).tolist()[0]
        min_g = min(g).tolist()[0]
        min_b = min(b).tolist()[0]

        #max
        max_h = max(h).tolist()[0]
        max_s = max(s).tolist()[0]
        max_v = max(v).tolist()[0]
        max_r = max(r).tolist()[0]
        max_g = max(g).tolist()[0]
        max_b = max(b).tolist()[0]

        #return normalised data for hsv and rgb seperately: [h, s, v, hrange, srange, vrange], [r, g, b, rrange, grange, brange]
        norm_h = min_h + (max_h-min_h)/2.0
        norm_ht = max_h - min_h + 2
        norm_s = min_s + (max_s-min_s)/2.0
        norm_st = max_s - min_s + 2
        norm_v = min_v + (max_v-min_v)/2.0
        norm_vt = max_v - min_v + 2

        norm_r = min_r + (max_r-min_r)/2.0
        norm_rt = max_r - min_r + 2
        norm_g = min_g + (max_g-min_g)/2.0
        norm_gt = max_g - min_g + 2
        norm_b = min_b + (max_b-min_b)/2.0
        norm_bt = max_b - min_b + 2

        return ([norm_h, norm_s, norm_v, norm_ht, norm_st, norm_vt],[norm_r, norm_g, norm_b, norm_rt, norm_gt, norm_bt])

    def normaliseThresholds(self, board, serialise = False):
        import boardstate
        for colour in HSV_Jewels.keys():
             
            ret = self._normaliseThresholdColour(colour, board)
            if ret is not None:
                HSV_Jewels[colour] = ret[0]
                RGB_Jewels[colour] = ret[1]
      

        if serialise:
            print 'Serialising...'
            import shutil
            import time
            import sys
            fname = 'thresholds'
            extra = time.asctime()
            extra = extra.replace(' ', '_')
            extra = extra.replace(':', '-')
            #copy off the old thresholds file
            shutil.copy(fname + '.py', fname + extra + '.py') 

            #redir print to a file
            saveout = sys.stdout
            fsock = open('thresholds.py', 'w')
            sys.stdout = fsock


        print 'HSV_Jewels={\\'
        colours = HSV_Jewels.keys()
        colours.sort()
        for colour in colours:
            ht = (colour, ) + tuple(HSV_Jewels[colour]) 
            print '\t\t\'%s\' :\t[%3.3f,%3.3f,%3.3f,%3.3f,%3.3f,%3.3f],' % (ht)
        print '}\n\n'
        
        print 'RGB_Jewels={\\'
        for colour in colours:
            rt = (colour, ) + tuple(RGB_Jewels[colour])
            print '\t\t\'%s\' :\t[%3.3f,%3.3f,%3.3f,%3.3f,%3.3f,%3.3f],' % (rt)
        print '}\n\n'
                
        if serialise:
            sys.stdout = saveout
            fsock.close()
            print 'Serialisation DONE'


    def rectFromMove(self, move):
        try:
            (col1, row1),(col2, row2) = move
        except Exception, e:
            print move
            print e

        return (col1 * self.xstep, row1*self.ystep, (col2-col1+1)*self.xstep, (row2-row1+1)*self.ystep)

    def get_hsv(self, r):
        from colorsys import rgb_to_hsv
        return rgb_to_hsv(r[0], r[1], r[2])
    
    def mapfn_1(self, data):
        
        
        h = 0.0
        s = 0.0
        v = 0.0
        r = 0.0
        g = 0.0
        b = 0.0

        cols = len(data)
        rows = len(data[0])
        l = float(cols * rows)
        
        #use all points
        for col in range(cols):
            hsvs = map(self.get_hsv, data[col])
            for entry in hsvs:
                h += entry[0]
                s += entry[1]
                v += entry[2]
            for row in range(rows):
                pixel = data[col][row]
                r += pixel[0]
                g += pixel[1]
                b += pixel[2]

        avg_h = float(h)/l
        avg_s = float(s)/l
        avg_v = float(v)/l
        avg_r = float(r)/l
        avg_g = float(g)/l
        avg_b = float(b)/l
        
        #Use only some points
        '''
        stat_data = []
        for col in range(cols):
            stat_data.append(get_hsv(data[col][0]))
            stat_data.append(get_hsv(data[col][rows/2]))
            stat_data.append(get_hsv(data[col][rows-1]))

        for stat in stat_data:
            h+=stat[0]
            s+=stat[1]
            v+=stat[2]
        avg_h = float(h)/float(cols*3)
        avg_s = float(s)/float(cols*3)
        avg_v = float(v)/float(cols*3)
        '''

        #print avg_h, avg_s, avg_v,
        retval = (JE.unknown, (avg_h, avg_s, avg_v), (avg_r, avg_g, avg_b))
        for key in HSV_Jewels.keys():
            h = HSV_Jewels[key][0]
            s = HSV_Jewels[key][1]
            v = HSV_Jewels[key][2]
            h_t = HSV_Jewels[key][3]
            s_t = HSV_Jewels[key][4]
            v_t = HSV_Jewels[key][5]
            
            r = RGB_Jewels[key][0]
            g = RGB_Jewels[key][1]
            b = RGB_Jewels[key][2]
            r_t = RGB_Jewels[key][3]
            g_t = RGB_Jewels[key][4]
            b_t = RGB_Jewels[key][5]

            if (h-h_t <= avg_h <= h+h_t) and\
               (s-s_t <= avg_s <= s+s_t) and\
               (v-v_t <= avg_v <= v + v_t)and\
               (r-r_t <= avg_r <= r+r_t)and\
               (g-g_t <=avg_g <= g+g_t)and\
               (b-b_t <= avg_b <= b+b_t):
                retval = (key, (avg_h, avg_s, avg_v), (avg_r, avg_g, avg_b))
                break
        return retval
        
    

    def _map_to_jewel(self):
        '''map via some function, a sampled area to one of the bejeweled jewels'''
        for row in range(self.rows):
            for col in range(self.cols):
                val = self.mapfn_1(self.samplearray[col][row])

                #print col, ' , ', row, ' : ', val

                self.jewelarray[col][row] = val
    
    def _sample_jewel_boxes(self):
        '''samples pixel boxes at each location'''
        
        for row in range(self.rows):
            for col in range(self.cols):
                #self.samplearray[col][row] = self.current_pixel_buffer.get_from_image(self.current_pixel_buffer, None, col * self.xstep, row * self.ystep, 0,0, self.sample_width, self.sample_height)
                gx, gy = self.get_gridpoint(col, row, offset = False)
                sample = self.current_pixel_buffer.subpixbuf(  gx - self.sample_width/2, gy - self.sample_height/2 , self.sample_width, self.sample_height)
                #save the image
                if self.dump_images:
                    sample.save("%s_%s.png" % (str(col), str(row) ), 'png')
               
                #get numpy representation
                arr = sample.get_pixels_array()
                #print col, ' , ', row, ' : ', arr
                self.samplearray[col][row] = arr
        self.dump_images = False

    def set_sample_box_size(self, width, height):
        '''size of each region to sample'''
        self.sample_width = width
        self.sample_height = height

    def get_gridpoint(self, col, row, offset = True):
        if not offset:
            x=0
            y=0
        else:
            x = self.x
            y = self.y
        gx = x + (col*self.xstep) + self.xstep/2
        gy = y + (row*self.ystep) + self.ystep/2
        return (gx, gy)

    def set_sample_grid(self, x, y, xstep, ystep, rows, cols):
        '''set the parameters of the grid
        x, y, x step, y step, rows, cols'''
        self.x = x
        self.y = y
        self.xstep = xstep
        self.ystep = ystep
        self.rows = rows
        self.cols = cols
        #from that, we can set the area of the 'window' to grab
        self.grab_width = x + (xstep * cols) + self.sample_width 
        self.grab_height = y + (ystep * rows) + self.sample_height 


    def get_jewel_grid(self, board = None):
        self._get_window_contents()
        self._sample_jewel_boxes()
        self._map_to_jewel()
        if board is not None:
            for col in range(self.cols):
                for row in range(self.rows):
                    board[col][row] = self.jewelarray[col][row][0] #0 is the JewelEnum

