import pygtk
pygtk.require('2.0')
import gtk
import pygame
from pygame.locals import *
import sys

if __name__ == "__main__":
    import pixelops

    print 'm to start looking for moves'
    print 'e to start doing moves'
    print 'arrows to move view window around'
    print 'space to dump what the view window sees'
    print 'n to normalise'
    print 'prgbwyo to expand colour threshold for gems'
    print 'PRGBWYO to expand colour threshold for sparkle gems'
    print '<ctrl> + prgbwyo to expand threshold for mults'
    
    
    pygame.init()
    w = 400
    h = 400
    screen = pygame.display.set_mode( (w,h) )
    background = pygame.Surface( (w,h) ).convert()
    clock = pygame.time.Clock()
    
    from jewelenum import JewelEnum as JE
    colorchoices = {    JE.redjewel             : (127,0,0), 
                        JE.redjewel_sparkle     : (192,0,0), 
                        JE.redjewel_multiplier  : (255,0,0), 
                        JE.purplejewel          : (127,0,127), 
                        JE.purplejewel_sparkle  : (192,0,192),
                        JE.purplejewel_multiplier: (255,0,255),
                        JE.yellowjewel          : (127,127,0),
                        JE.yellowjewel_sparkle  : (192,192,0),
                        JE.yellowjewel_multiplier  : (255,255,0),
                        JE.whitejewel           : (64,64,64),
                        JE.whitejewel_sparkle   : (192,192,192),
                        JE.whitejewel_multiplier: (255,255,255),
                        JE.greenjewel           : (0,127,0),
                        JE.greenjewel_sparkle   : (0,192,0),
                        JE.greenjewel_multiplier : (0,255,0),
                        JE.bluejewel            : (0,0,127),
                        JE.bluejewel_sparkle    : (0,0,192),
                        JE.bluejewel_multiplier    : (0,0,255),
                        JE.orangejewel          : (127, 64, 0),
                        JE.orangejewel_sparkle  : (192, 127, 0),
                        JE.orangejewel_multiplier  : (255, 192, 0),
                        JE.hyperball            : (0,0,0),
                        JE.unknown              : (127,127,127)
                    }


    def loadimage(name):
        import pygame
        import pygame.image
        return pygame.image.load('images/' + name + '.png')

    imagechoices = {    JE.redjewel             : loadimage('red') ,
                        JE.redjewel_sparkle     : loadimage('red_sp'), 
                        JE.redjewel_multiplier  : loadimage('red_mult'),
                        JE.purplejewel          : loadimage('purple'), 
                        JE.purplejewel_sparkle  : loadimage('purple_sp'),
                        JE.purplejewel_multiplier: loadimage('purple_mult'),
                        JE.yellowjewel          : loadimage('yellow'),
                        JE.yellowjewel_sparkle  : loadimage('yellow_sp'),
                        JE.yellowjewel_multiplier :loadimage('yellow_mult'),
                        JE.whitejewel           : loadimage('white'),
                        JE.whitejewel_sparkle   : loadimage('white_sp'),
                        JE.whitejewel_multiplier: loadimage('white'),
                        JE.greenjewel           : loadimage('green'),
                        JE.greenjewel_sparkle   : loadimage('green'),
                        JE.greenjewel_multiplier : loadimage('green_mult'),
                        JE.bluejewel            : loadimage('blue'),
                        JE.bluejewel_sparkle    : loadimage('blue_sp'),
                        JE.bluejewel_multiplier :  loadimage('blue_mult'),
                        JE.orangejewel          : loadimage('orange'),
                        JE.orangejewel_sparkle  : loadimage('orange_sp'),
                        JE.orangejewel_multiplier  : loadimage('orange_mult'),
                        JE.hyperball            : loadimage('hyperball'),
                        JE.unknown              : loadimage('unknown')
                    }

    from boardstate import BoardState
    gameboard = BoardState(8,8)

    bestmove = (0,0),(0,0)
    othermoves = []
    dt = 0
    check_frequency = 50
    check_counter = 0
    move_frequency = 1000
    move_counter = 0
    find_moves = False
    do_moves = False
    game_counter = 0
    game_frequency = 62000

    j = pixelops.JewelSampler()
    j.set_sample_grid(179, 207, 40, 40, 8, 8)
    j.set_sample_box_size(10,12)
    j.get_jewel_grid(board = gameboard.board)

    while True:
        dt = clock.tick(30) #20 fps is fine

        j.get_jewel_grid(board = gameboard.board)
        
        for row in range(j.rows):
            for col in range(j.cols):
                jj = j.jewelarray[col][row][0]
                colour = colorchoices[jj]
                
                #background.fill(colour, (col * j.xstep, row * j.ystep, j.xstep, j.ystep))
       
                #draw image
                background.blit(imagechoices[jj], (col *j.xstep, row*j.ystep), None)

        keystate = pygame.key.get_pressed()

        for event in pygame.event.get():
            if event.type == QUIT:
                sys.exit()
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                sys.exit()
            elif event.type == KEYDOWN and event.key == K_SPACE:
                for row in range(j.rows):
                    for col in range(j.cols):
                        name, (h,s,v), (r,g,b) = j.jewelarray[col][row]
                        print "[%d,%d]:%s (%f,%f,%f) (%f,%f,%f)" % (col,row,name,h,s,v, r, g, b)
                j.dump_images = True
            elif event.type == KEYDOWN and event.key == K_m: #start looking for moves
                find_moves = not find_moves
                game_counter = 0
            elif event.type == KEYDOWN and event.key == K_e:  #start doing moves
                do_moves = not do_moves
        
            #normalise
            elif event.type == KEYDOWN and event.key == K_n:
                if keystate[K_LSHIFT]:
                    j.normaliseThresholds(gameboard.board, serialise = True)
                else:
                    j.normaliseThresholds(gameboard.board)
          
            elif event.type == KEYDOWN and event.key == K_UP:
                j.y -=1
            elif event.type == KEYDOWN and event.key == K_DOWN:
                j.y +=1
            elif event.type == KEYDOWN and event.key == K_RIGHT:
                j.x +=1
            elif event.type == KEYDOWN and event.key == K_LEFT:
                j.x -=1

            #expand threshold:
            elif event.type == KEYDOWN:
                if event.key == K_p:       #purple
                    if keystate[K_LSHIFT]:  #shiny
                        j.expandThreshold(JE.purplejewel_sparkle)
                    elif keystate[K_LCTRL]: #multiplier
                        j.expandThreshold(JE.purplejewel_multiplier)
                    else:                  #standard
                        j.expandThreshold(JE.purplejewel)
                
                elif event.key == K_r:       #red
                    if keystate[K_LSHIFT]:  #shiny
                        j.expandThreshold(JE.redjewel_sparkle)
                    elif keystate[K_LCTRL]: #multiplier
                        j.expandThreshold(JE.redjewel_multiplier)
                    else:                  #standard
                        j.expandThreshold(JE.redjewel)
                
                elif event.key == K_b:       #blue
                    if keystate[K_LSHIFT]:  #shiny
                        j.expandThreshold(JE.bluejewel_sparkle)
                    elif keystate[K_LCTRL]: #multiplier
                        j.expandThreshold(JE.bluejewel_multiplier)
                    else:                  #standard
                        j.expandThreshold(JE.bluejewel)

                elif event.key == K_g:       #green
                    if keystate[K_LSHIFT]:  #shiny
                        j.expandThreshold(JE.greenjewel_sparkle)
                    elif keystate[K_LCTRL]: #multiplier
                        j.expandThreshold(JE.greenjewel_multiplier)
                    else:                  #standard
                        j.expandThreshold(JE.greenjewel)

                elif event.key == K_y:       #yellow
                    if keystate[K_LSHIFT]:  #shiny
                        j.expandThreshold(JE.yellowjewel_sparkle)
                    elif keystate[K_LCTRL]: #multiplier
                        j.expandThreshold(JE.yellowjewel_multiplier)
                    else:                  #standard
                        j.expandThreshold(JE.yellowjewel)

                elif event.key == K_w:       #white
                    if keystate[K_LSHIFT]:  #shiny
                        j.expandThreshold(JE.whitejewel_sparkle)
                    elif keystate[K_LCTRL]: #multiplier
                        j.expandThreshold(JE.whitejewel_multiplier)
                    else:                  #standard
                        j.expandThreshold(JE.whitejewel)
                
                elif event.key == K_o:       #orange
                    if keystate[K_LSHIFT]:  #shiny
                        j.expandThreshold(JE.orangejewel_sparkle)
                    elif keystate[K_LCTRL]: #multiplier
                        j.expandThreshold(JE.orangejewel_multiplier)
                    else:                  #standard
                        j.expandThreshold(JE.orangejewel)

                
        check_counter+=dt
        game_counter += dt
        move_counter += dt
        if game_counter > game_frequency:
            #print 'Time exceeded!', game_counter
            find_moves = False
        if check_counter > check_frequency and find_moves:
            check_counter = 0
            bestmove, othermoves = gameboard.evaluate_sweep()
            if move_counter > move_frequency and do_moves:
                move_counter = 0
                gameboard.perform_move(bestmove, j)
       
        if find_moves or True:
            for move in othermoves:
                pygame.draw.rect(background, (40,40,40), j.rectFromMove(move), 2 )
            pygame.draw.rect(background, (255,255,255), j.rectFromMove(bestmove), 3)

    
        screen.blit(background, (0,0) )
        pygame.display.flip()

