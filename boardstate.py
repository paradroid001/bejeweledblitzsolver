#boardstate.
#holds the state of the board in a numpy array. enum in each.
from weightings import weights

from jewelenum import JewelEnum

import Xlib
from Xlib import X, display

class BoardState(object):
    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self.board = []
        self.display = display.Display()
        self.screen = self.display.screen()
        self.root = self.screen.root

        #init the board
        for col in range(cols):
            self.board.append([])
            for row in range(rows):
                self.board[col].append(None)

    def _evaluate(self, col, row, vertical = True):
        colour_match_words = self.board[col][row].split('_')
        score = 0
        score_colour = ''
        nummatched = 0
        check_col = col
        check_row = row
        #check down or right
        matched = True
        if colour_match_words[0] == JewelEnum.unknown:
            #print 'Unknown!'
            return [0, [JewelEnum.unknown] ]
        
        while matched:
            if vertical:
                check_row += 1
                if check_row >= self.rows:
                    matched = False
                    break
            else:
                check_col += 1
                if check_col >= self.cols:
                    matched = False
                    break

            check_colour_string = self.board[check_col][check_row]
            check_colour_words = check_colour_string.split('_')
            if check_colour_words[0] == JewelEnum.unknown:
                #print 'UNKNOWN!'
                matched = False
                break
    
            if not matched:
                break

            #check for match and assign points
            if check_colour_words[0] == colour_match_words[0]:
                score_colour = colour_match_words[0]
                score += weights[check_colour_string] #TODO: proper scoring
                matched = True
                #print score_colour
                nummatched += 1
            else:
                matched = False
        #check up or left
        matched = True
        check_col = col
        check_row = row
        while matched: 
            if vertical:
                check_row -= 1
                if check_row < 0:
                    matched = False
                    break
            else:
                check_col -= 1
                if check_col < 0:
                    matched = False
                    break
            check_colour_string = self.board[check_col][check_row]
            check_colour_words = check_colour_string.split('_')
            if check_colour_words[0] == JewelEnum.unknown:
                matched = False
                break

            if not matched:
                break

            #check for match and assign points
            if check_colour_words[0] == colour_match_words[0]:
                score_colour = colour_match_words[0]
                #print score_colour
                match = True
                score += weights[check_colour_string] #TODO: proper scoring
                nummatched += 1
            else:
                matched = False
    
        #todo: correct hyperball logic
        #todo: 5 in a row logic
        if nummatched <2 and colour_match_words[0] is not JewelEnum.hyperball: #2 would mean a match of 3
            score = 0
        else:
            #add on score for lower rows
            score += row
            if nummatched >=4 :
                score *= 4
            #print 'Match!: (', col, row, colour_match_words[0], '): ', score 
        return (score, '_'.join(colour_match_words) )


    def evaluate_move(self, (col1, row1), (col2, row2)):
        ''' for a given move and boardstate, return a value
            representing how good that move is.
            can only use data for known chains
            *amount of prospective new moves created,
            *probability of further chains
            to produce a score
        '''
        score = 0
        if self.board[col1][row1] == self.board[col2][row2]:
            return (0, JewelEnum.unknown)
        #swap the jewels temporarily
        temp = self.board[col1][row1]
        self.board[col1][row1] = self.board[col2][row2]
        self.board[col2][row2] = temp

        #evaluate jewel1
        score1, colour1 = self._evaluate(col1, row1, vertical = True)
        score2, colour2 = self._evaluate(col1, row1, vertical = False)
        #evaluate jewel2
        score3, colour3 = self._evaluate(col2, row2, vertical = True)
        score4, colour4 = self._evaluate(col2, row2, vertical = False)

        #swap them back
        self.board[col2][row2] = self.board[col1][row1]
        self.board[col1][row1] = temp

        return (score1+score2+score3+score4, [colour1, colour2, colour3, colour4])


    def evaluate_sweep(self):
        '''sweeps through the board evaluating all moves'''
        moves = {}
        highest_score = 0
        best_move = (0,0),(0,1)
        other_moves = []
        #moves is a dict which looks like this:
        #(col1,row1, col2,row2) : score
        for row in range(self.rows-1): #all except last row
            for col in range(self.cols):
                move =  (col, row), (col, row+1) 
                score, colours = self.evaluate_move( *move )
                if score > 0:
                    moves[move] = score
                    other_moves.append(move)
                    if score > highest_score:
                        highest_score = score
                        #print 'found better move: ',
                        best_move = move
                        #print best_move, colours, score

        for col in range(self.cols-1): #all except last col
            for row in range(self.rows):
                move =  (col, row), (col+1, row) 
                score, colours = self.evaluate_move( *move )
                if score > 0:
                    moves[move] = score
                    other_moves.append(move)
                    if score > highest_score: 
                        highest_score = score
                        #print 'found better move: ',
                        best_move = move 
                        #print best_move, colours, score
        
        
        return (best_move, other_moves)

    def mouse_click(self, button): #button= 1 left, 2 middle, 3 right
        Xlib.ext.xtest.fake_input(self.display,Xlib.X.ButtonPress, button)
        self.display.sync()
        Xlib.ext.xtest.fake_input(self.display,Xlib.X.ButtonRelease, button)
        self.display.sync()


    def perform_move(self, move, pixops, virtual=False):
        '''actually performa a move, applying bejeweled logic.
        if non-virtual, then the move should be performed via the mouse,
        and the result written back to the boardstate
        writing back to the boardstate may not be necessary - we resample the
        boardstate on a set frequency anyway'''
        ( (col1, row1), (col2, row2) ) = move
        p1 = pixops.get_gridpoint(col1, row1, offset = True)
        p2 = pixops.get_gridpoint(col2, row2, offset = True)
        self.root.warp_pointer(*p1)
        self.display.sync()
        self.mouse_click(1)
        self.root.warp_pointer(*p2)
        self.display.sync()
        self.mouse_click(1)

        #now get back to our window...
        self.root.warp_pointer(1200, 700)
        self.display.sync()
        self.mouse_click(1)
