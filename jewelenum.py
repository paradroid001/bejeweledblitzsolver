class JewelEnum:
    redjewel            = 'red'
    orangejewel         = 'orange'
    bluejewel           = 'blue'
    greenjewel          = 'green' 
    yellowjewel         = 'yellow' 
    whitejewel          = 'white' 
    purplejewel         = 'purple'
    
    redjewel_sparkle    = 'red_sp'
    orangejewel_sparkle = 'orange_sp'
    bluejewel_sparkle   = 'blue_sp' 
    greenjewel_sparkle  = 'green_sp' 
    yellowjewel_sparkle = 'yellow_sp' 
    whitejewel_sparkle  = 'white_sp' 
    purplejewel_sparkle = 'purple_sp'
    
    redjewel_multiplier     = 'red_mu'
    orangejewel_multiplier  = 'orange_mu'
    bluejewel_multiplier    = 'blue_mu'
    greenjewel_multiplier   = 'green_mu'
    yellowjewel_multiplier  = 'yellow_mu'
    whitejewel_multiplier   = 'white_mu'
    purplejewel_multiplier  = 'purple_mu'
    
    unknown             = 'unknown'
    
    hyperball           = 'hyperball'

